import React, { Component } from 'react'

import Gmap from '../components/Gmap'
import Sidebar from '../components/Sidebar'


export class App extends Component {


  render() {
    return (
      <div className="app">
        <Sidebar />
        <Gmap gMapKey={this.props.gMapKey} />
      </div>
    );
  }
}

export default App