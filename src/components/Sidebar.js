import React, { Component } from 'react'
import {connect} from 'react-redux'
import SearchBlock from './SearchBlock'
import Profile from '../components/sidebar/Profile'

class Sidebar extends Component {

  activeComponent = () => {
    if(this.props.sidebar.activeProfile !== false) return <Profile profile={this.props.sidebar.activeProfile} />
  }
  

  //El Sidebar renderiza la busqueda o un perfil
  render() {
    return (
      <div>
        <SearchBlock />
        {this.activeComponent()}
      </div>
    );
  }
}

function mapStateToProps({sidebar}){
  return {
    sidebar
  }
}

export default connect(mapStateToProps)(Sidebar)