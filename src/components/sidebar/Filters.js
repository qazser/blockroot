import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

class SearchBlock extends Component {

  handleToggle = value => () => {
    const checked = this.props.filter;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked,
    }, () => this.props.updateFilter(this.state.checked));
  };

  render() {
    const { classes } = this.props;
    return (
        <div>
            <List style={styles.filterRow}>
            {['Solo ofertas', 'Minoristas', 'Mayoristas'].map(value => (
                <ListItem
                key={value}
                role={undefined}
                dense
                button
                onClick={this.handleToggle(value)}
                className={classes.listItem}
                style={styles.filterRow}
                >
                <Checkbox
                    checked={this.props.filter.indexOf(value) !== -1}
                    tabIndex={-1}
                    disableRipple
                />
                <ListItemText primary={value} />
                </ListItem>
            ))}
            </List>
        </div>
      )
  }
}

const styles = {
  card: {
    width: 320,
    position: 'absolute',
    zIndex: 10,
    top:10,
    left:10
  },
  search: {
    padding:10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  searchI: {
    cursor: 'pointer',
    display: 'inline',
  },
  filterI: {
    cursor: 'pointer',
    display: 'inline',
  },
  filterRow: {
    paddingTop:0,
    paddingBottom:0
  }
}


export default withStyles(styles)(SearchBlock)