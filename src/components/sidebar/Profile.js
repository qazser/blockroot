import React, { Component } from 'react'
import moment from 'moment'
import { withStyles } from '@material-ui/core/styles'
import Avatar from '@material-ui/core/Avatar'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Grid from '@material-ui/core/Grid'
import 'typeface-roboto'

class Profile extends Component {

  renderSchedule() {
    const { classes, profile } = this.props;
    if (profile.schedule && profile.schedule.length > 0) {
      const dias = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo']
      const horarios = profile.schedule.split('/')
      const horasObj = horarios.map((horarr, i) => {
        const horas = horarr.split(',')

        let tablaHoras
        if (horas[2] !== 'C') {
          //Si es formato 1 columna
          tablaHoras = {
            dia: dias[i],
            hora1: horas[2] !== 'C' ? 'C: '+horas[2] : 'Cerrado',
            hora2: ''
          }
        } else {
          //Si es formato 2 columnas
          tablaHoras = {
            dia: dias[i],
            hora1: horas[0] !== 'C' ? 'M: '+horas[0] : 'Cerrado',
            hora2: horas[1] !== 'C' ? 'T: '+horas[1] : 'Cerrado'
          }
        }
        return tablaHoras
      })

      return (
        <div>
          <Typography gutterBottom variant="body2" component="h3">
            Horarios
          </Typography>

          <Table className={classes.table}>
            <TableBody>
              {horasObj.map(row => {
                return (
                  <TableRow key={row.dia} className={classes.tablerow}>
                    <TableCell className={classes.tableHcell} component="th" scope="row">
                      {row.dia}
                    </TableCell>
                    <TableCell className={classes.tablecell} numeric>{row.hora1}</TableCell>
                    <TableCell className={classes.tablecell} numeric>{row.hora2}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </div>
      )
    } else {
      return (
        <div>
          <Typography gutterBottom variant="body2" component="h3">
            Horarios
        </Typography>
          <Typography component="p" variant="body2">
            No hay horario establecido
        </Typography>
        </div>
      )
    }
  }

  renderOffers() {
    const { classes, profile } = this.props;

    if (profile.total_offers > 0) {

      const icon_oferta = require('../../assets/img/icon-oferta80x80.png')

      const offersjsx = profile.offers.map((offer, ind) => (
        <div className={ind !== 0 && classes.offerBox} >
          <Typography gutterBottom variant="body2" component="h3">
            Oferta
          </Typography>
          <Grid container key={ind}>
            <Grid item xs={2}>
              <Avatar src={icon_oferta} />
            </Grid>
            <Grid item xs={10}>
              <Typography component="p">
                {offer.description}
              </Typography>
              <Typography component="p" className={classes.offerDate}>
                Desde {moment(offer.start).format('DD/MM/YYYY')} hasta  {moment(offer.end).format('DD/MM/YYYY')}
              </Typography>
              <Typography component="p" paragraph={true} gutterBottom >
                {offer.discount}
              </Typography>
            </Grid>

          </Grid>
          <CardMedia
            className={classes.offerMedia}
            image={offer.image}
          />
        </div>
      ))

      return (
        <div>
          {offersjsx}
        </div>
      )
    }
  }

  render() {
    const { classes, profile } = this.props;
    if (profile.company_image === '') profile.company_image = require('../../assets/img/icon-perfil-default.png')
    if (profile.public_image === '') profile.public_image = require('../../assets/img/350x150.jpg')

    return (
      <Card style={styles.card}>
        <CardMedia
          className={classes.media}
          image={profile.public_image}
        />
        <CardContent>
          <Grid container spacing={24}>
            <Grid item xs={2}>
              <Avatar src={profile.company_image} />
            </Grid>
            <Grid item xs={10}>
              <Typography gutterBottom variant="title" component="h3">
                {profile.name}
              </Typography>
              <Typography component="p">
                {profile.street}, {profile.address_number}
              </Typography>
              <Typography component="p">
                {profile.city}, {profile.country}
              </Typography>
              <Typography component="p">
                {profile.description}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              {this.renderSchedule()}
            </Grid>
            <Grid item xs={12}>
              {this.renderOffers()}
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    )
  }
}

const styles = {
  card: {
    width: 340,
    position: 'absolute',
    zIndex: 5,
    top: 0,
    left: 0,
    maxHeight: 445,
    overflowY: 'auto'
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  offerBox: {
    marginTop: 125
  },
  offerMedia: {
    position: 'absolute',
    left: 0,
    right: 0,
    paddingTop: '34.25%'
  },
  offerDate: {
    fontSize: '.7rem'
  },
  tablerow: {
    height: 32
  },
  tableHcell: {
    padding: 0
  },
  tablecell: {
    padding: '0 !important',
    fontSize: '.7rem'
  },
  avatar: {
    margin: 10,
  },
}


export default withStyles(styles)(Profile);
