import React, { Component } from 'react'
import Result from './Result'
class ResultList extends Component {

  renderSearchResults = () => {
      return this.props.shops.map((shop, i) => (
          <Result profile={shop} key={i} showProfile={this.props.showProfile} />
      ))
  }

  render() {
    return (
        <div style={styles.resultList}>
            {this.renderSearchResults()}
        </div>
    )
  }
}

const styles = {
    resultList: {
        maxHeight: 350,
        overflowY: 'auto'
    }
}

export default ResultList