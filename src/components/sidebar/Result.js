import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import BusinessIcon from '@material-ui/icons/Business'

class Result extends Component {

  render() {
    const {classes,profile} = this.props

    if (profile.public_image === '') profile.public_image = require('../../assets/img/350x150.jpg')

    if(!profile.categories) profile.categories = [profile.description]

    return (
        <Card style={styles.card}>
          <CardMedia
            className={classes.media}
            image={profile.public_image}
          />
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={9}>
                <Typography gutterBottom variant="title" component="h3">
                    {profile.name}
                </Typography>
                <Typography component="p">
                    {profile.categories.join(', ')}
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <Button variant="fab" color="primary" aria-label="Business" mini={true} className={classes.button} onClick={() => this.props.showProfile(profile)}>
                    <BusinessIcon />
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      );  
  }
}

const styles = {
    card: {
      width: 320,
    },
    media: {
      height: 0,
      paddingTop: '35%', // 16:9
    }
  }

export default withStyles(styles)(Result)