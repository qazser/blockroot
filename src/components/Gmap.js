import React, { Component } from 'react'
import { connect } from 'react-redux'
import GoogleMapReact from 'google-map-react'
import { loadShops } from '../actions/search_actions'
import { showProfile, hideProfile } from '../actions/sidebar_actions'

let oms;
let markers = [];
let OverlappingMarkerSpiderfier;

function createMapOptions(maps) {
    return {
        zoom: 13,
        streetViewControl: false,
        scaleControl: false,
        overviewMapControl: false,
        mapTypeControl: false,
        fullscreenControl: false,
        panControl: false,
        zoomControl: true,
    };
}

class Gmap extends Component {
    state = {
        map: null,
        maps: null,
        mounted: false
    };

    static defaultProps = {
        zoom: 13
    };

    componentDidMount() {
        this.props.loadShops()
        this.setState({
            mounted: true
        });
    }

    showProfile = (profile) => {
        this.props.showProfile(profile)
    }

    loadMarkers = () => {
        return this.props.shops.shops.map((c, i) => (
            { position: { lat: c.latitude, lng: c.longitude }, profile: c, icon: this.showMarkerIcon(c.total_offers) }
        ))
    }

    showMarkerIcon = (offers) => {
        if (offers > 0) return require('../assets/img/marker2810.png')
        else return require('../assets/img/marker2811.png')
    }

    componentDidUpdate(prevProps, prevState) {
        const { map, maps } = this.state;

        const { showComposePin: prevShowComposePin } = prevProps;
        const { showComposePin, composeCoord } = this.props;

        // need to make sure map exists before any of the marker actions can be done
        if (map !== null && maps !== null) {
            // first time drop pin
            if (showComposePin && !prevShowComposePin) {
                this.dropMarkerPin(map, maps, this.props.coord);
            }

            // update existing pin
            if (
                showComposePin &&
                prevShowComposePin &&
                composeCoord.lat !== null &&
                composeCoord.lng !== null
            ) {
                if (
                    prevProps.composeCoord.lat !== composeCoord.lat ||
                    prevProps.composeCoord.lng !== composeCoord.lng
                ) {
                    this.clearMarkerPin(() => {
                        this.dropMarkerPin(map, maps, composeCoord);
                    });
                }
            }

            // do not drop pin anymore
            if (prevShowComposePin && !showComposePin) {
                this.clearMarkerPin();
            }
        }
    }

    initMap(map, maps) {
        OverlappingMarkerSpiderfier = require("overlapping-marker-spiderfier");

        this.setState({
            map,
            maps
        });
    }

    mapOnChange = map => {
        //console.log("mapOnChange", map);
    };

    render() {
        const projects = this.loadMarkers();

        if (this.state.mounted > 0) {
            if (OverlappingMarkerSpiderfier != null) {
                const options = {
                    keepSpiderfied: true,
                    nearbyDistance: 20 //in pixels
                };
                oms = new OverlappingMarkerSpiderfier(this.state.map, options);

                const google = window.google

                //remove any previous marker
                markers.map(ma => {
                    ma.setMap(null);
                    return true;
                })
                oms.clearMarkers()

                //Create new marker
                markers = projects.map(p => {
                    const marker = new google.maps.Marker({
                        position: { lat: p.position.lat, lng: p.position.lng },
                        map: this.state.map,
                        icon: p.icon,
                        profile: p.profile
                    });
                    oms.addMarker(marker);
                    return marker
                });

                oms.addListener("spiderfy", function (markers) {
                });

                oms.addListener("click", async marker => {
                    await new Promise(resolve => setTimeout(resolve, 100));
                    this.props.showProfile(marker.profile)
                });
            }
        }

        return (
            <div style={{ height: "100%", width: "100%", position: "absolute" }}
            >
                {this.state.mounted > 0 && (
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: this.props.gMapKey }}
                        center={{ lat: 41.4131564, lng: 2.2021936 }}
                        zoom={this.props.zoom}
                        options={createMapOptions}
                        onGoogleApiLoaded={({ map, maps }) => this.initMap(map, maps)}
                        onChange={this.mapOnChange}
                        onClick={this.props.hideProfile}
                        yesIWantToUseGoogleMapApiInternals
                    >
                    </GoogleMapReact>
                )}
            </div>
        );
    }
}


function mapStateToProps({ shops }) {
    return {
        shops
    }
}


export default connect(mapStateToProps, { loadShops, showProfile, hideProfile })(Gmap)
