import React, { Component } from 'react'
import {connect} from 'react-redux'
import {loadShops,searchShops, updateFilter} from '../actions/search_actions'
import {showProfile} from '../actions/sidebar_actions'
import {withStyles} from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import Input from '@material-ui/core/Input'
import SearchIcon from '@material-ui/icons/Search'
import FilterIcon from '@material-ui/icons/FilterList'
import Filters from './sidebar/Filters'
import ResultList from './sidebar/ResultList'


class SearchBlock extends Component {
  state = {
    filters: false,
    searchQuery: ''
  }

  updateFilter = (filter) => {
    this.props.updateFilter(filter)
    this.props.loadShops(filter)
  }

  displayFilter = () => {
    if(this.state.filters === true)
    {
      return <Filters filter={this.props.shops.filter} updateFilter={this.updateFilter} />
    }
  }

  displaySearchResults = () => {
    if(this.props.sidebar.searchResults === true)
    {
      return <ResultList shops={this.props.shops.shops} showProfile={this.props.showProfile} />
    }
  }


  _handleKeyPress = e => {
    if (e.key === 'Enter') {
      this.handleSubmitSearch()
    }
  }

  handleChangeSearchQuery = event => {
    this.setState({searchQuery: event.target.value});
  }

  handleSubmitSearch = () => {
    if(this.state.searchQuery.length > 0) this.props.searchShops(this.state.searchQuery, this.state.filter)
    else this.props.loadShops(this.state.filter)
  }

  render() {
    return (
      <Card style={styles.card}>
        <div style={styles.search}>
          <SearchIcon style={styles.searchI} onClick={this.handleSubmitSearch} />
          <Input
            disableUnderline
            placeholder="Buscar…"
            value={this.state.searchQuery} 
            onChange={this.handleChangeSearchQuery}
            onKeyPress={this._handleKeyPress}
          />
          <FilterIcon onClick={() => (this.setState({filters: !this.state.filters}))} style={styles.filterI} />
        </div>
        {this.displayFilter()}
        {this.displaySearchResults()}
      </Card>
    );
  }
}

const styles = {
  card: {
    width: 320,
    position: 'absolute',
    zIndex: 10,
    top:10,
    left:10
  },
  search: {
    padding:10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  searchI: {
    marginRight: 15,
    cursor: 'pointer',
    display: 'inline',
  },
  filterI: {
    cursor: 'pointer',
    display: 'inline',
  },
  filterRow: {
    paddingTop:0,
    paddingBottom:0
  }
}

function mapStateToProps({shops, sidebar}){
    return {
      shops,
      sidebar
    }
}

export default connect(mapStateToProps, {loadShops,searchShops,showProfile, updateFilter})(withStyles(styles)(SearchBlock));