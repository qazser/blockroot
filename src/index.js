import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {createStore,applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import App from './components/App'
import reducers from './reducers'

const middleware = [thunk]
const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);

const gMapKey= document.getElementById('br-maps').getAttribute("gmap-api-key")

ReactDOM.render(
  <Provider  store={createStoreWithMiddleware(reducers)}>
    <App gMapKey={gMapKey} />
  </Provider>,
  document.getElementById('br-maps')
)