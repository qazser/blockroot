import {
  LOAD_SHOPS, UPDATE_FILTER} from '../actions/types'

const initialState ={
  shops: [],
  filter: []
}

export default function (state=initialState, action) {
  switch(action.type) {
    case LOAD_SHOPS:
      return {...state, shops: action.payload.shops }
    case UPDATE_FILTER:
      return {...state, filter: action.payload.filter }
    default:
      return state
  }
}