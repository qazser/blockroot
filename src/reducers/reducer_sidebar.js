import {LOAD_PROFILE, SHOW_SEARCH_RESULTS, HIDE_PROFILE} from '../actions/types'

const initialState ={
  activeProfile: false,
  searchResults: false
}


export default function (state=initialState, action) {
  switch(action.type) {
    case LOAD_PROFILE:
      return {...initialState, activeProfile: action.payload.profile }
    case SHOW_SEARCH_RESULTS:
      return {...initialState, searchResults:true}
    case HIDE_PROFILE:
      return {...initialState}
    default:
      return state
  }
}