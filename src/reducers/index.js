import {combineReducers} from 'redux'
import ShopsReducer from './reducer_shops'
import SidebarReducer from './reducer_sidebar'



const rootReducer = combineReducers({
  shops: ShopsReducer,
  sidebar: SidebarReducer
})

export default rootReducer