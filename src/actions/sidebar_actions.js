import {
    LOAD_PROFILE,
    HIDE_PROFILE 
    } from './types'
    
  
  export const showProfile = profile => async dispatch => {
    
    dispatch({
      type: LOAD_PROFILE,
      payload: {profile}
    }) 
  }

  export const hideProfile = () => async dispatch => {
    dispatch({
      type: HIDE_PROFILE    
    }) 
  }