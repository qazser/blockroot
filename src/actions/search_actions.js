import {
    LOAD_SHOPS, 
    SHOW_SEARCH_RESULTS,
    UPDATE_FILTER
    } from './types'
    
  
  export const loadShops = (filter = []) => async dispatch => {

    //Filter
    let qpart = ''

    const filterkeys = {
      'Solo ofertas': 'only_offers',
      'Mayoristas': 'wholesale',
      'Minoristas': 'retailer'
    }

    if(filter.length > 0){
      const qarr = filter.map((fl) => (`${filterkeys[fl]}=1`))
      qpart = '?'+qarr.join('&')
    }
    
    //API Call basic search
    const response = await fetch('https://api.rec.barcelona/public/map/v1/list'+qpart)
    const data = await response.json();
    const shops = data.data.elements
  
    dispatch({
      type: LOAD_SHOPS,
      payload: {shops}
    })
  }

  export const searchShops = (query, filter = []) => async dispatch => {

    //Filter
    let qpart = ''

    const filterkeys = {
      'Solo ofertas': 'only_offers',
      'Mayoristas': 'wholesale',
      'Minoristas': 'retailer'
    }

    if(filter.length > 0){
      const qarr = filter.map((fl) => (`${filterkeys[fl]}=1`))
      qpart = '&'+qarr.join('&')
    }
    
    //API Call basic search
    const response = await fetch(`https://api.rec.barcelona/public/map/v1/search?search=${query}${qpart}`)
    const data = await response.json();
    const shops = data.data.elements
  
    dispatch({
      type: LOAD_SHOPS,
      payload: {shops}
    })

    dispatch({
      type: SHOW_SEARCH_RESULTS
    })
  }

  export const updateFilter = (filter) => async dispatch => {

    await dispatch({
      type: UPDATE_FILTER,
      payload: {filter}
    })
  }