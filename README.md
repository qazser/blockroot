Este proyecto ofrece a REC Barcelona un mapa con sus negocios afiliados a través de una API.


## Instalación

Para instalar el proyecto es necesario descargarlo o clonarlo desde este repositorio.

Comando para clonar el proyecto. Es necesario tener git y node.js V8 o superior instalado en la máquina de desarrollo.
```shell
git clone https://bitbucket.org/qazser/blockroot/  rec-bcn
cd rec-bcn
```

Tras descargar una copia del proyecto es necesario instalar los paquetes de los que depende.

```shell
npm install
```

## Vista previa de desarrollo

Para ver la aplicación web directamente en el navegador, use

```shell
npm start
```


## Crear una versión compilada y actualizar el plugin de wordpress

Tras hacer cambios en el código ReactJS, y testear su funcionamiento, se ha de crear una versión compilada de la aplicación usando la siguiente línea de comando

```shell
npm run build
```

Esto reemplaza la carpeta build que hay en la raíz por una nueva con los archivos actualizados.

Cuando finalice dicho proceso, se debe actualizar el plugin reemplazando la carpeta build en la carpeta br-maps-wp-plugin por la carpeta build recientemente creada en la raíz.

Finalmente se puede comprimir la carpeta br-maps-wp-plugin en un archivo .zip, eliminar el plugin en el wordpress en el que esté instalado e instalar el nuevo.