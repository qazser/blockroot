<?php

add_shortcode( 'rec-cmap', 'widget_rec_cmap' );

//[rec-cmap"]
function widget_rec_cmap($atts) {

    $a = shortcode_atts( array(
      'gmap-api-key' => '',
    ), $atts );

    $gMapKey = $a['gmap-api-key'];


    $CSSfiles = scandir(dirname(__FILE__) . '/../build/static/css/');
    foreach($CSSfiles as $filename) {
      if(strpos($filename,'.css')&&!strpos($filename,'.css.map')) {
        wp_enqueue_style( 'recbarcelona_cmap_Plugin_css', plugin_dir_url( __FILE__ ) .'../build/static/css/' . $filename, array(), mt_rand(10,1000), 'all' );
      }
    }

    //Archivo css compatibilidad diseño avada
    wp_enqueue_style( 'recbarcelona_cmap_Plugin_css_main', plugin_dir_url( __FILE__ ) .'../css/br-maps.css', array(), mt_rand(10,1000), 'all' );


    $JSfiles = scandir(dirname(__FILE__) . '/../build/static/js/');
    $react_js_to_load = '';
    foreach($JSfiles as $filename) {
      if(strpos($filename,'.js')&&!strpos($filename,'.js.map')) {
        $react_js_to_load = plugin_dir_url( __FILE__ ) . '../build/static/js/' . $filename;
      }
    }
    
    // React dynamic loading
    wp_enqueue_script('recbarcelona_cmap_Plugin_react', $react_js_to_load, '', mt_rand(10,1000), true);

    //Cargamos la vista de la tabla
    ob_start();
    require(sprintf("%s/../views/mapaLocales.php", dirname(__FILE__)));
    return ob_get_clean();
}
