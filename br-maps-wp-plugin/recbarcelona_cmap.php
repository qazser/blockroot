<?php
/*
Plugin Name: recbarcelona cmap
Plugin URI: https://rec.barcelona
Description: Plugin para generar un mapa de comercios sincronizadamente mediante una API
Author URI: https://int3rim.eu/
Text Domain: recb-cmap
Version: 1.0
Author: Int3rim Consulting
License: GPLv2
*/
if(!class_exists('recbarcelona_cmap_Plugin'))
{
	class recbarcelona_cmap_Plugin
	{
		/**
		 * Construct the plugin object
		 */
		public function __construct()
		{

		//Shortcodes
		require_once(sprintf("%s/controllers/shortcodeController.php", dirname(__FILE__)));


		}

		/**
		 * Activate the plugin
		 */
		public static function activate()
		{
			// Do nothing
		}

		/**
		 * Deactivate the plugin
		 */
		public static function deactivate()
		{
			// Do nothing
		}
	}
}

if(class_exists('recbarcelona_cmap_Plugin'))
{
	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('recbarcelona_cmap_Plugin', 'activate'));
	register_deactivation_hook(__FILE__, array('recbarcelona_cmap_Plugin', 'deactivate'));

	// instantiate the plugin class
	$recbarcelona_cmap_Plugin = new recbarcelona_cmap_Plugin();
}
